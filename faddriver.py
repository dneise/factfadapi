import readline, rlcompleter
readline.parse_and_bind('tab: complete')

import socket
import threading 
import Queue
import struct
import sys
import select
import time

import numpy as np



commands = {
    'CMD_DENABLE': 0x06, 
    'CMD_DDISABLE': 0x07,
    
    'CMD_DWRITE_RUN': 0x08,
    'CMD_DWRITE_STOP': 0x09,
    
    'CMD_TRIGGERS_ON': 0x18,
    'CMD_TRIGGERS_OFF': 0x19,

    'CMD_MODE_COMMAND': 0x30,
    'CMD_MODE_ALL_SOCKETS': 0x31,

    'CMD_MANUAL_BUSY_ON': 0x32,
    'CMD_MANUAL_BUSY_OFF': 0x33,
    'CMD_BUSY_FIX_OFF_TRUE': 0x24,
    'CMD_BUSY_FIX_OFF_FALSE': 0x25,

    'CMD_SCLK_ON': 0x10,
    'CMD_SCLK_OFF': 0x11,

    'CMD_SRCLK_ON':0x15,
    'CMD_SRCLK_OFF': 0x16,
    #------------------------------------------
    'CMD_TRIGGER': 0xA0,
    'CMD_RESET_TRIGGER_ID': 0x2A,

    'CMD_EXECUTE': 0x04,
    'CMD_WRITE': 0x05,

    'CMD_READ': 0x0A,
    'CMD_PS_DIRINC': 0x12,
    'CMD_PS_DIRDEC': 0x13,
    'CMD_PS_DO': 0x14,
    'CMD_PS_RESET': 0x17,
    
    'CMD_START': 0x22,
    'CMD_STOP': 0x23,
    
    'CMD_TRIGGER_C': 0x1F,
    'CMD_TRIGGER_S': 0x20,
    'CMD_STATUS': 0xFF}

 
class FadDriver(threading.Thread):
    
    def __init__(self, ip, debugging=True, timeout=30):
        """ connects itself to address=('ip.add.re.ss')
        
        send data to endpoint using ClientSocketThread.command_queue.
        stop thread by setting Event ClientSocketThread.stop
        
        gather data by accessing the ClientSocketThread.event_queue.
        """
        #super(ClientSocketThread, self).__init__(self)
        threading.Thread.__init__(self) 
        
        self.cmd_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.cmd_socket.connect( (ip, 31919) )
        self.data_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.data_socket.connect( (ip, 31920) )
        
        # power on defaults
        self.hardware_status = dict( 
            denable = False,
            dwrite = True,
            trigger_line = False,
            output_socket = "cmd_socket",
            busy = 'normal',
            spi_bus = True,
            drs_shift_register_clock = True,
            
            firmware_major = 2,
            firmware_minor = 14,
            temperatures = None,
            reference_clock_frequency = None,
            DNA = None,
            ADC_clk_phase_shift = None,
            crate_id = None,
            board_id = None,
            rois = np.ones( 36, dtype=np.uint16) * 1024,
            package_length_in_bytes = (int((np.ones( 36, dtype=np.uint16) * 1024).sum()) * 2) + (36*8) + 72 + 4,
            runnumber = 0,
            continous_trigger_prescaler = 0,
        )
        
        self.set_domino_wave(True)
        time.sleep(1)
        self.set_domino_write(True)
        time.sleep(1)
        self.set_spi_bus(True)
        time.sleep(1)
        self.set_domino_shift_register_clock(True)
        time.sleep(1)
        # 0x0022, CMD_START
        self.set_busy('normal')
        time.sleep(1)
        self.set_output_socket('data_socket')
        time.sleep(1)

        
        self.buffer = bytearray(100 * self.hardware_status['package_length_in_bytes'])
        self.view = memoryview(self.buffer)
        self.read_pointer = 0
        self.write_pointer = 0
        
        self.data_queue = Queue.Queue()
        
    def run(self):
        #shortcuts
        view = self.view
        rd = self.read_pointer
        wr = self.write_pointer
        while True:
            # try to read some bytes from data_socket
            n_bytes = self.data_socket.recv_into( view[wr:] )
            #print >>sys.stderr,"read", n_bytes, "bytes"
            wr += n_bytes
            
            #print "wr"
            # in case we have enough bytes, create FadPackage and push it into the queue
            if wr-rd >= self.hardware_status['package_length_in_bytes']:
                print time.time()
                package = create_package(self.buffer[rd:wr])
                self.data_queue.put( package )
                rd += self.hardware_status['package_length_in_bytes']
                
            self.read_pointer = rd
            self.write_pointer = wr
        

# SETTERS --------------------------------------------------------------------
    def set( self, **kwargs ):
        """ convenience setter ... 
            domino_wave = bool,
            domino_write = bool,
            trigger_line = bool,
            output_socket = "cmd_socket" or "data_socket",
            busy = "normal", "always" or "never",
            spi_bus = bool,
            domino_shift_register_clock = bool, # experts only
            reset_event_counter = bool
            rois = int, #or list-like of ints,
            dacs = [8 ints],
            runnumber = int,
            continuous_trigger_prescaler = int )
        """
        impossible_while_datataking = [
            'rois',                    # because hardware can't do it 
            'dacs', 
            'runnumber', 
            'continuous_trigger_prescaler',
            'output_socket']            # because API doesn't like it :-)
        always_possible = [
            'domino_wave', 
            'domino_write', 
            'busy', 
            'spi_bus', 
            'domino_shift_register_clock',
            'reset_event_counter',
            'trigger_line'
            ]
        
        for key in kwargs:
            if key in always_possible:
                getattr(self, 'set_'+key)(kwargs[key])

        # check if user wanted something, which is only possible 
        # when *not* taking data
        need_to_switch_trigger_off = False
        for key in kwargs:
            if key in impossible_while_datataking:
                need_to_switch_trigger_off = False
                break
        if need_to_switch_trigger_off:
            current_trigger_line = self.hardware_status['trigger_line']
            set_trigger_line(False)
            for key in kwargs:
                if key in impossible_while_datataking:
                    getattr(self, 'set_'+key)(kwargs[key])
            set_trigger_line(current_trigger_line)
        

    # ALWAYS POSSIBLE SETTERS ------------------------------------------------
    def set_domino_wave(self, status ):
        """ send command to alter status of DENABLE line of FAD
        """
        if status == True:
            s = struct.pack('H',commands['CMD_DENABLE'])
        else:
            s = struct.pack('H',commands['CMD_DDISABLE'])
        self.__send_bytes_to_board(s)
        self.hardware_status['denable'] = status

    def set_domino_write(self, status ):
        """ send command to alter status of DWRITE line of FAD
        """
        if status == True:
            s = struct.pack('H',commands['CMD_DWRITE_RUN'])
        else:
            s = struct.pack('H',commands['CMD_DWRITE_STOP'])
        self.__send_bytes_to_board(s)
        self.hardware_status['dwrite'] = status
    
    def set_busy(self, behaviour ):
        """ send command to alter behaviour of the busy signal
        
        behaviour: either of "always", "never", "normal"
        """
        if behaviour == 'always':
            s1 = struct.pack('H',commands['CMD_MANUAL_BUSY_ON'])
            s2 = struct.pack('H',commands['CMD_BUSY_FIX_OFF_FALSE'])
            s = s1 + s2
        elif behaviour == 'never':
            s1 = struct.pack('H',commands['CMD_MANUAL_BUSY_OFF'])
            s2 = struct.pack('H',commands['CMD_BUSY_FIX_OFF_TRUE'])
            s = s1 + s2
        elif behaviour == 'normal':
            s1 = struct.pack('H',commands['CMD_MANUAL_BUSY_OFF'])
            s2 = struct.pack('H',commands['CMD_BUSY_FIX_OFF_FALSE'])
            s = s1 + s2
        else:
            raise ValueError("behaviour must bei either of 'always', 'never' or 'normal'.")
        self.__send_bytes_to_board(s)
        self.hardware_status['busy'] = behaviour
    def set_spi_bus(self, status ):
        """ send command to switch on or off the entire SPI bus
        """
        if status == True:
            s = struct.pack('H',commands['CMD_SCLK_ON'])
        else:
            s = struct.pack('H',commands['CMD_SCLK_OFF'])
        self.__send_bytes_to_board(s)
        self.hardware_status['spi_bus'] = status

    def set_domino_shift_register_clock(self, status ):
        """ expert command to enable or disable the DRS shift register clock.
        """
        if status == True:
            s = struct.pack('H',commands['CMD_SRCLK_ON'])
        else:
            s = struct.pack('H',commands['CMD_SRCLK_OFF'])
        self.__send_bytes_to_board(s)
        self.hardware_status['drs_shift_register_clock'] = status
    def set_reset_event_counter( self ):
        s = struct.pack('H',commands['CMD_RESET_TRIGGER_ID'])
        self.__send_bytes_to_board(s)

    def set_trigger_line(self, status ):
        """ send command to allow or disallow processing of external triggers
        """
        if status == True:
            s = struct.pack('H',commands['CMD_TRIGGERS_ON'])
        else:
            s = struct.pack('H',commands['CMD_TRIGGERS_OFF'])
        self.__send_bytes_to_board(s)
        self.hardware_status['trigger_line'] = status

    # ONLY POSSIBLE WHEN TRIGGERS OFF ----------------------------------------
    def set_rois(self, rois):
        """ send commands to set ROIs of all 36 channels
        
        rois: either an integer or an array like of length 36
        
        in case rois is an integer, all channels get the same roi
        else the rois get set according to the fields in rois.
        """
        rois = np.array(rois).astype(np.uint16)
        
        if len(rois) == 1:
            rois = np.ones( 36, dtype=np.uint16) * rois[0]

        if len(rois) != 36:
            raise ValueError('rois must be either integer, or list like of length 36.')
        
        if (rois > 1024).any() or (rois < 0).any():
            raise ValueError('all ROIs must be between 0 and 1024')

        s = ""
        for i,roi in enumerate(rois):
            cmd = struct.pack('B', commands['CMD_WRITE'])
            address = struct.pack('B', i)
            roi = struct.pack('H', rois[i])
            s += cmd + address + roi
        s += struct.pack('H', commands['CMD_EXECUTE'])
        
        self.hardware_status['rois'] = rois
        self.__send_bytes_to_board(s)
    
    def set_dacs( self, dacs=[25000, 0, 0, 0, 28800, 28800, 28800, 28800]):
        """ send commands to set DAC channels
        dacs : a list like of length 8.
            default: [25000, 0, 0, 0, 28800, 28800, 28800, 28800]
        """
        address_offset = 36
        
        dacs = np.array(dacs, dtype=np.uint16)
        if len(rois) != 8:
            raise ValueError('dacs must be list like of length 8')        

        s = ""
        for i,dac in enumerate(dacs):
            cmd = struct.pack('B', commands['CMD_WRITE'])
            address = struct.pack('B', i+address_offset)
            dac = struct.pack('H', dacs[i])
            s += cmd + address + roi
        s += struct.pack('H', commands['CMD_EXECUTE'])
        
        self.hardware_status['dacs'] = dacs
        self.__send_bytes_to_board(s)
    
    def set_runnumber( self, runnumber):
        cmd = struct.pack('B', commands['CMD_WRITE'])
        address_msb = struct.pack('B', 45)
        address_lsb = struct.pack('B', 46)
        runnumber = struct.pack('I', runnumber)
        execute_cmd = struct.pack('H', commands['CMD_EXECUTE'])
        s = cmd + address_msb + runnumber[2:] + cmd + address_lsb + runnumber[:2] + execute_cmd
        self.hardware_status['runnumber'] = struct.unpack('I', runnumber)[0]
        self.__send_bytes_to_board(s)
    
    def set_continous_trigger_prescaler(self, value ):
        cmd = struct.pack('B', commands['CMD_WRITE'])
        address = struct.pack('B', 44)
        value_str = struct.pack('H', value)
        execute_cmd = struct.pack('H', commands['CMD_EXECUTE'])
        s = cmd + address + value_str + execute_cmd
        self.hardware_status['continous_trigger_prescaler'] = value
        self.__send_bytes_to_board(s)

    def set_output_socket(self, name ):
        """ send command to specify, which socket to use for outputting data packages
        name: "cmd_socket" or "data_socket"
        """
        if name == 'cmd_socket':
            s = struct.pack('H',commands['CMD_MODE_COMMAND'])
        elif name == 'data_socket':
            s = struct.pack('H',commands['CMD_MODE_ALL_SOCKETS'])
        else:
            raise ValueError("name must bei either of 'cmd_socket' or 'data_socket'.")
            
        self.__send_bytes_to_board(s)
        self.hardware_status['output_socket'] = name

    def software_trigger( self ):
        s = struct.pack('H',commands['CMD_TRIGGER'])
        self.__send_bytes_to_board(s)
        
# PRIVATE --------------------------------------------------------------------
    def __send_bytes_to_board( self, bytes):
        """ low level utility function to send some raw bytes to the FAD using the command socket.
        """
        #print "maybe I need to lock and release a lock, when using the socket for writing..."
        self.cmd_socket.sendall(bytes)
        #print "I would like to trigger as soft-trigger at this point at compare the header, with the expectation value"
    
    def __del__(self):
        print "destructor wird gerufen"
        self.sock.close()
# SETTERS END-----------------------------------------------------------------
        
# GETTERS --------------------------------------------------------------------
    def get( name ):
        try:
            return self.hardware_status['name']
        except KeyError:
            print "possible names are:", self.hardware_status.keys()
            print 
            raise
# GETTERS END---------3--------------------------------------------------------

class FadPackage(object):
    def __init__(self, header, body, footer ):
        self.header = header
        self.body = body
        self.footer = footer
def create_package(bytes):
    #shortcut
    d = str(bytes)    
    header, body, footer = get_header_channel_footer_types( roi=1024 )
    
    header = np.fromstring( d[:header.itemsize], dtype=header)
    d = d[header.dtype.itemsize:]
    body = np.fromstring( d[:body.itemsize*36], dtype=body )
    d = d[body.dtype.itemsize*36:]
    footer = np.fromstring( d[:footer.itemsize], dtype=footer )
    return FadPackage( header, body, footer)
def get_header_channel_footer_types( roi=1024 ):
    header_type =  np.dtype([
            ('start_package_flag', '>u2'),
            ('package_length', '>u2'),
            ('version_no', '>u2'),
            ('PLLLCK', '>u2'),
            
            ('FTM interface timeout', '>u1'),
            ('trigger_crc', '>u1'),
            ('trigger_type', '>u2'),
            ('trigger_id', '>u4'),
            
            ('fad_evt_counter', '>u4'),

            ('REFCLK_frequency', '>u4'),
            
            ('board_id', '>u2'),
            ('zeroes', '>u1'),
            ('adc_clock_phase_shift', '>i1'),
            ('number_of_triggers_to_generate', '>u2'),
            ('trigger_generator_prescaler', '>u2'),
            
            ('DNA', '>u8'),
            
            ('time', '>u4'),
            ('runnumber', '>u4'),
            
            ('drs_temperature', '>i2', (4,)),

            ('dac', '>u2', (8,)),
            ])
    channel_type = np.dtype([
            ('channel_info','>u2'),
            ('start_cell','>u2'),
            ('roi', '>u2'),
            ('zeroes', '>u2'), # so the channel header is 8 bytes .. not 6.
            ('data', '<i2', (roi,) )
            ])
    footer_type = np.dtype([ ('cre','>u2'), ('endflag', '>u2') ])
    return header_type, channel_type, footer_type


# TEST -----------------------------------------------------------------

fad = FadDriver("10.0.128.128")
fad.setDaemon(True)
fad.start()
fad.software_trigger()
