import readline
import rlcompleter
import re
readline.parse_and_bind('tab: complete')
readline.parse_and_bind('Control-Space: complete')
readline.set_completer_delims(re.compile(r'[\'"\\[]').sub('', readline.get_completer_delims())) 

import numpy as np
import matplotlib.pyplot as plt
plt.ion()

import cPickle

import sys

d = cPickle.load( open( sys.argv[1] ) )[0]

from pprint import pprint

import paket
paket.roi = 1024

header = np.fromstring( d[0:paket.header.dtype.itemsize], dtype=paket.header.dtype )
body = np.fromstring( d[paket.header.dtype.itemsize:paket.header.dtype.itemsize+paket.body.dtype.itemsize*36], dtype=paket.body.dtype )
footer = np.fromstring( d[-paket.footer.dtype.itemsize:], dtype=paket.footer.dtype )

paket.print_header( header )

for b in body:
    print "%04x %5d %5d" % (b['channel_info'], b['roi'], b['start_cell']) , b['data']

print 

for name in footer.dtype.names:
    print "%15s 0x%04x" % (name, footer[name])

plt.plot(body['data'].transpose(), ':.')
plt.xlim(-12,1024+12)
