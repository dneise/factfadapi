import socket
import sys
import threading 
import Queue 
import cmd 
import time
import struct

commands = {
    'CMD_EXECUTE': 0x04,
    'CMD_WRITE': 0x05,
    'CMD_DENABLE': 0x06,
    'CMD_DDISABLE': 0x07,
    'CMD_DWRITE_RUN': 0x08,
    'CMD_DWRITE_STOP': 0x09,
    'CMD_READ': 0x0A,
    'CMD_SCLK_ON': 0x10,
    'CMD_SCLK_OFF': 0x11,
    'CMD_PS_DIRINC': 0x12,
    'CMD_PS_DIRDEC': 0x13,
    'CMD_PS_DO': 0x14,
    'CMD_SRCLK_ON':0x15,
    'CMD_SRCLK_OFF': 0x16,
    'CMD_PS_RESET': 0x17,
    'CMD_TRIGGERS_ON': 0x18,
    'CMD_TRIGGERS_OFF': 0x19,
    'CMD_RESET_TRIGGER_ID': 0x2A,
    'CMD_START': 0x22,
    'CMD_STOP': 0x23,
    'CMD_MODE_COMMAND': 0x30,
    'CMD_MODE_ALL_SOCKETS': 0x31,
    'CMD_MANUAL_BUSY_ON': 0x32,
    'CMD_MANUAL_BUSY_OFF': 0x33,
    'CMD_TRIGGER': 0xA0,
    'CMD_TRIGGER_C': 0x1F,
    'CMD_TRIGGER_S': 0x20,
    'CMD_BUSY_FIX_OFF_TRUE': 0x24,
    'CMD_BUSY_FIX_OFF_FALSE': 0x25,
    'CMD_STATUS': 0xFF}

# default values
ip_address = '10.0.128.128'
ports = [31919, 31920]   # -- each FAD opens 2 ports, and we need to connect them both! 
#ip_address = 'localhost'
#ports = [10000]   # -- each FAD opens 2 ports, and we need to connect them both! 

# list of sockets

# for each socket we have one output buffer, so we can store the sockets output 
# seperated from each other.
# Since we receive a string on bytes from each socket .. the output is simply stores in ... well ... strings
# We identify a socket using its fileno() method. So we will use a dict of strings here

socks = {}
buffers = {}
addresses = []
for port in ports:
    address = (ip_address, port)
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socks[address] = sock
    buffers[address] = ""
    addresses.append(address)


# This is an awesome little function, I have written for PyDimCtrl once
# I creates methods for the given class at run time... really nice.
def add_do_cmd(class_name, command_name): 
    method_name = "do_" + command_name.lower() 
    def new_method(self, *args):
        self._cmd(command_name, *args)
    new_method.__name__ = method_name
    new_method.__doc__="currently no help available \n"
    setattr( class_name, new_method.__name__, new_method)

import select



class MeineKonsole(cmd.Cmd):

    def __init__(self): 
        cmd.Cmd.__init__(self) 
        self.prompt = "fad: "
        for command in commands.keys():
            add_do_cmd( MeineKonsole, command)
    
    def _cmd(self, command_name, *args):
        print "chosen command:", command_name
        print "sending: 0x%04x" %(commands[command_name], )
        s = struct.pack('H',commands[command_name] )
        socks[addresses[0]].sendall( s )
        print "successfully sent it."


    def do_read_all(self,prm):
        while True:
            read,write,error = select.select( socks.values(), [],[],0.5)
            for sock in read:
                print sock.recv(1024)   
            if not read:
                break

    def do_setup( self, prm):
        numbers = [0x0006, 0x0008, 0x0010, 0x0015, 0x0022, 0x0025, 0x0033, 0x0028, 0x0030]
        for number in numbers:
            s = struct.pack('H',number)
            socks[addresses[0]].sendall(s)
            print "sending:", [hex(ord(si)) for si in s]
            time.sleep(1)
 
    def do_send_long(self, prm):
        number = int(prm, base=0) # base = 0 means it should guess the base
        s = struct.pack('L',number)
        print "sending:", [hex(ord(si)) for si in s]
        socks[addresses[0]].sendall(s)
        print "sent"
    
    def do_send_int(self, prm):
        number = int(prm, base=0) # base = 0 means it should guess the base
        s = struct.pack('I',number)
        print "sending:", [hex(ord(si)) for si in s]
        print "sending:", s
        socks[addresses[0]].sendall(s)
        print "sent"

    def do_send_short(self, prm):
        number = int(prm, base=0) # base = 0 means it should guess the base
        s = struct.pack('H',number)
        print "sending:", [hex(ord(si)) for si in s]
        print "sending:", s
        socks[addresses[0]].sendall(s)
        print "sent"

    def do_send_string( self, prm):
        print "sending"
        socks[addresses[0]].sendall( prm)
        print "sent"

    def do_print(self, prm):
        if prm:
            address = addresses[int(prm)]
            print "printing buffer of socket %d, fileno: %d" % address
            print buffers[address]
        else:
            for address in socks.keys():
                print "printing buffer of socket %d, fileno: %d" % address
                print buffers[address]

    def do_connect(self, prm):
        # Connect the socket to the port where the server is listening
        for address in sorted(socks.keys()):
            sock = socks[address]
            print >>sys.stderr, 'connecting to %s port %s' % address
            sock.connect(address)
            print >>sys.stderr, 'connected to %s port %s fileno %d' % tuple(list(address) + [sock.fileno()])

    def do_disconnect(self, prm):
        for address in socks.keys():
            print >>sys.stderr, 'disconnecting to %s port %s' % address
            sock = socks[address]
            sock.close()

    def do_reconnect( self, prm):
        self.do_disconnect(prm)
        self.do_connect(prm)

    def do_purge_mem( self, prm):
        if prm:
            address = addresses[int(prm)]
            print "purging buffer of socket %d, fileno: %d" % address
            buffers[address] = ""
        else:
            for address in socks.keys():
                print "purging buffer of socket %d, fileno: %d" % address
                buffers[address] = ""

    def do_exit(self, prm): 
        print "Auf Wiedersehen" 
        return True    
    def do_EOF(self, prm): 
        return self.do_exit( prm )
    def emptyline(self):
        # one needs to overwrite this method, because the default 
        # is to repeat the last command in case an empty line is entered.
        return False


    
konsole = MeineKonsole() 


try:
    konsole.cmdloop()
finally:
    print >>sys.stderr, 'closing sockets'
    for address in socks.keys():
        sock = socks[address]
        sock.close()

