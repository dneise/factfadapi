import numpy as np

roi = 1024

def print_header( header ):
    h = header
    print "start_package_flag    0x%04x" % h['start_package_flag']
    print "package_length(bytes) %d" % (h['package_length'].astype(int)*2, )
    print "version_no            0x%04x" % h['version_no']
    print "PLLLCK                0x%04x" % h['PLLLCK']
    print "trigger_crc           0x%04x" % h['trigger_crc']
    print "trigger_type          %d" % h['trigger_type'] 
    print "trigger_id            %d" % h['trigger_id'] 
    print "fad_evt_counter       %d" % h['fad_evt_counter'] 
    print "REFCLK_frequency      %d" % h['REFCLK_frequency'] 
    print "board_id              0x%04x" % h['board_id'] 
    print "adc_clock_phase_shift %d" % h['adc_clock_phase_shift'] 
    print "number_of_triggers_to_generate %d" % h['number_of_triggers_to_generate'] 
    print "trigger_generator_prescaler %d" % h['trigger_generator_prescaler'] 
    print "DNA                   0x%08x" % h['DNA'] 
    print "time                  %d" % h['time'] 
    print "runnumber             %d" % h['runnumber'] 
    print "drs_temperature(deg)  %r" % (h['drs_temperature']/16. , )
    print "dac %r" % h['dac'] 

header =  np.recarray(1, dtype = np.dtype([
            ('start_package_flag', '>u2'),
            ('package_length', '>u2'),
            ('version_no', '>u2'),
            ('PLLLCK', '>u2'),
            
            ('FTM interface timeout', '>u1'),
            ('trigger_crc', '>u1'),
            ('trigger_type', '>u2'),
            ('trigger_id', '>u4'),
            
            ('fad_evt_counter', '>u4'),
            ('REFCLK_frequency', '>u4'),
            
            ('board_id', '>u2'),
            ('zeroes', '>u1'),
            ('adc_clock_phase_shift', '>i1'),
            ('number_of_triggers_to_generate', '>u2'),
            ('trigger_generator_prescaler', '>u2'),
            
            ('DNA', '>u8'),
            
            ('time', '>u4'),
            ('runnumber', '>u4'),
            
            ('drs_temperature', '>i2', (4,)),

            ('dac', '>u2', (8,)),
            
            ]))

body = np.recarray( 1, dtype = np.dtype([

            ('channel_info','>u2'),
            ('start_cell','>u2'),
            ('roi', '>u2'),
            ('zeroes', '>u2'), # so the channel header is 8 bytes .. not 6.
            ('data', '<i2', (roi,) )
            ]))

footer =  np.recarray( 1, dtype = np.dtype([
            
            ('cre','>u2'),
            ('endflag', '>u2') 

            ] ) )  
            

"""
#define NTemp          4
#define NDAC           8


typedef struct {


} __attribute__((__packed__)) PEVNT_HEADER;

typedef struct {
uint16_t id;
uint16_t start_cell;
uint16_t roi;
uint16_t filling;
 int16_t adc_data[];
} __attribute__((__packed__)) PCHANNEL;


typedef struct {
uint16_t package_crc;
uint16_t end_package_flag;
} __attribute__((__packed__)) PEVNT_FOOTER;

"""
