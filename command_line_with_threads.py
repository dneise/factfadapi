import readline, rlcompleter
readline.parse_and_bind('tab: complete')

import sys
import cmd 
import time
import paket 

from client_socket_thread import ClientSocketThread

commands = {
    'CMD_EXECUTE': 0x04,
    'CMD_WRITE': 0x05,
    'CMD_DENABLE': 0x06,
    'CMD_DDISABLE': 0x07,
    'CMD_DWRITE_RUN': 0x08,
    'CMD_DWRITE_STOP': 0x09,
    'CMD_READ': 0x0A,
    'CMD_SCLK_ON': 0x10,
    'CMD_SCLK_OFF': 0x11,
    'CMD_PS_DIRINC': 0x12,
    'CMD_PS_DIRDEC': 0x13,
    'CMD_PS_DO': 0x14,
    'CMD_SRCLK_ON':0x15,
    'CMD_SRCLK_OFF': 0x16,
    'CMD_PS_RESET': 0x17,
    'CMD_TRIGGERS_ON': 0x18,
    'CMD_TRIGGERS_OFF': 0x19,
    'CMD_RESET_TRIGGER_ID': 0x2A,
    'CMD_START': 0x22,
    'CMD_STOP': 0x23,
    'CMD_MODE_COMMAND': 0x30,
    'CMD_MODE_ALL_SOCKETS': 0x31,
    'CMD_MANUAL_BUSY_ON': 0x32,
    'CMD_MANUAL_BUSY_OFF': 0x33,
    'CMD_TRIGGER': 0xA0,
    'CMD_TRIGGER_C': 0x1F,
    'CMD_TRIGGER_S': 0x20,
    'CMD_BUSY_FIX_OFF_TRUE': 0x24,
    'CMD_BUSY_FIX_OFF_FALSE': 0x25,
    'CMD_STATUS': 0xFF}

# default values
ip_address = '10.0.128.128'
ports = [31919, 31920]   # -- each FAD opens 2 ports, and we need to connect them both! 

# list of sockets

addresses = []
socks = []
for port in ports:
    address = (ip_address, port)
    addresses.append(address)

data = []
# This is an awesome little function, I have written for PyDimCtrl once
# I creates methods for the given class at run time... really nice.
def add_do_cmd(class_name, command_name): 
    method_name = "do_" + command_name.lower() 
    def new_method(self, *args):
        self._cmd(command_name, *args)
    new_method.__name__ = method_name
    new_method.__doc__="currently no help available \n"
    setattr( class_name, new_method.__name__, new_method)

class MeineKonsole(cmd.Cmd):

    def __init__(self): 
        cmd.Cmd.__init__(self) 
        self.prompt = "fad: "
        for command in commands.keys():
            add_do_cmd( MeineKonsole, command)
    
    def _cmd(self, command_name, *args):
        socks[0].command_queue.put( commands[command_name] )
        
    def do_send(self, prm):
        number = int(prm, base=0) # base = 0 means it should guess the base
        socks[0].command_queue.put( number )
        
    def do_print(self, prm):
        for i,sock in enumerate(socks):
            print "printing buffer of socket", i
            sock.bufferLock.acquire()
            for x in sock.buffer:
                print hex(ord(x)),
            print
            print "number_of_bytes:", len(sock.buffer)
            data.append( sock.buffer) 
            sock.bufferLock.release()
    def do_setup(self, prm):
        numbers = [0x0006, 0x0008, 0x0010, 0x0015, 0x0022, 0x0025, 0x0033, 0x0028, 0x0030]
        for number in numbers:
            socks[0].command_queue.put( number )

                
    def do_connect(self, prm):
        # Connect the socket to the port where the server is listening
        for address in addresses:
            sock = ClientSocketThread(address)
            sock.start()
            socks.append(sock)
            
    def do_disconnect(self, prm):
        for sock in socks:
            print >>sys.stderr, 'telling threads to shut down.'
            sock.stop.set()
        
    def do_purge_mem( self, prm):
        for sock in socks:
            sock.bufferLock.acquire()
            sock.buffer = ""
            sock.bufferLock.release()

    def do_exit(self, prm): 
        print "Auf Wiedersehen" 
        for sock in socks:
            print >>sys.stderr, 'telling threads to shut down.'
            sock.stop.set()
        return True    
    def do_EOF(self, prm): 
        return self.do_exit( prm )
    def emptyline(self):
        # one needs to overwrite this method, because the default 
        # is to repeat the last command in case an empty line is entered.
        return False

konsole = MeineKonsole()
#try:
konsole.cmdloop()
#finally:
#    print >>sys.stderr, 'asking socks tu shut down'
#    for i,sock in enumerate(socks):
#        print "stopping sock",i
#        sock.stop.set()
#        print "joining sock",i
#        sock.join()
