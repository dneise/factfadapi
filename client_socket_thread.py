import socket
import threading 
import Queue
import struct
import sys
import select
import time
 
class ClientSocketThread(threading.Thread): 
    def __init__(self, address, debugging=True, timeout=30):
        """ connects itself to address=('ip.add.re.ss', port)
        
        send data to endpoint using ClientSocketThread.command_queue.
        stop thread by setting Event ClientSocketThread.stop
        
        gather data by accessing the ClientSocketThread.event_queue.
        """
        
        #super(ClientSocketThread, self).__init__(self)
        threading.Thread.__init__(self) 
        self.debugging = debugging
        
        self.address = address
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(timeout)
        if debugging:
            print >>sys.stderr, 'connecting to %s port %s' % self.address
        self.sock.connect(self.address)
        if debugging:
            print >>sys.stderr, 'connected to %s port %s fileno %d' % tuple(list(address) + [self.sock.fileno()])

        self.buffer = bytearray(100*74092) # 100 of the largest events....
        view = memoryview(self.buffer)
        self.command_queue = Queue.Queue()
        self.stop = threading.Event()
    
    def run(self): 
        while True:
            # We do always 2 things in the while loop:
            #   1.) check if we have to send something... and send it... 
            #   2.) check if we have to read something ... and read it...
            #
            # We never stop doing this, thus we are killing a lot of CPU... but who cares?
            
            # First part is here----------------------------------------------
            if not self.command_queue.empty():
                command = self.command_queue.get()
                s = struct.pack('H',command)
                # this can block, I guess, when the other end is not listening anymore
                try:
                    if self.debugging:
                        print >>sys.stderr, 'sending ', [hex(ord(si)) for si in s]
                    self.sock.sendall(s)
                    self.command_queue.task_done()
                    time.sleep(1)
                    if self.debugging:
                        print >>sys.stderr, 'completely sent'
                except socket.timeout:
                    print >>sys.stderr, 'timeout occured for socket %r' % (address,)
                    return
            
            # Second part is here---------------------------------------------
            # We just read one little piece, not everything
            ready_to_read, ready_to_write, in_error = select.select([self.sock],[],[self.sock],0.5)
            #print >>sys.stderr, self.address, ready_to_read,
            if ready_to_read:
                n_bytes = self.sock.recv_into( view )
                view = view[n_bytes:]
                
                self.bufferLock.acquire()
                self.buffer += self.sock.recv(8*4096)
                self.bufferLock.release()
            
            if in_error:
               print >>sys.stderr, in_error
               return
            
            # I am not sure if I have to protect the check if a threading.Event.is_set() 
            # like any critical section, or if the Event is doing that by itself.
            if self.stop.is_set():
                return
 
    def __del__(self):
        if self.debugging:
            print >>sys.stderr, 'disconnecting from %s port %s' % self.address
        self.sock.close()
